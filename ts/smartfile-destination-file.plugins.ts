// node native scope
import * as fs from 'fs';
import * as path from 'path';

export {
  fs,
  path
};

// pushrocks scope
import * as smartfile from '@pushrocks/smartfile';
import * as smartlogInterfaces from '@pushrocks/smartlog-interfaces';
import * as smarttime from '@pushrocks/smarttime';

export {
  smartfile,
  smartlogInterfaces,
  smarttime
};
