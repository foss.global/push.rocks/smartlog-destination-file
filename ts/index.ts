import * as plugins from './smartfile-destination-file.plugins';

export class SmartlogDestinationFile implements plugins.smartlogInterfaces.ILogDestination {
  public fileWriteStream: plugins.fs.WriteStream;

  public async handleLog(logPackageArg: plugins.smartlogInterfaces.ILogPackage) {
    this.fileWriteStream.write(`${new plugins.smarttime.ExtendedDate(Date.now()).toISOString()}: ${logPackageArg.message} \n`);
  }

  constructor(filePathArg: string) {
    const extendedDate = new plugins.smarttime.ExtendedDate(Date.now());
    if (!plugins.path.isAbsolute(filePathArg)) {
      throw new Error(`filePath needs to be absolute but is not: "${filePathArg}"`);
    }
    plugins.smartfile.fs.ensureFileSync(filePathArg, `# Smartlogfile. Created at ${extendedDate.toISOString()}\n`);
    this.fileWriteStream = plugins.fs.createWriteStream(
      filePathArg,
      {
        flags: 'a+',
      }
    );
  }
}
