import { expect, tap } from '@pushrocks/tapbundle';
import * as smartlogDestinationFile from '../ts/index';

import * as path from 'path';

const outputFilePath = path.join(__dirname, 'output.txt');
console.log(outputFilePath);

let testSmartlogDestinationFile: smartlogDestinationFile.SmartlogDestinationFile;

tap.test('should create a valid instance of ', async () => {
  testSmartlogDestinationFile = new smartlogDestinationFile.SmartlogDestinationFile(outputFilePath);
});

tap.test('should write to the log file', async () => {
  testSmartlogDestinationFile.handleLog({
    context: null,
    correlation: {
      id: '123',
      type: 'build'
    },
    level: 'info',
    message: 'this is a log message',
    timestamp: Date.now(),
    type: 'log',
    data: null
  });
});

tap.start();
